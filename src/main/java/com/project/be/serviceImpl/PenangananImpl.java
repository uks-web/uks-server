package com.project.be.serviceImpl;

import com.project.be.repository.PenangananRepository;
import com.project.be.service.PenangananService;
import com.project.be.exception.NotFoundException;
import com.project.be.model.Penanganan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PenangananImpl implements PenangananService {
    @Autowired
    private PenangananRepository penangananRepository;

    @Override
    public Penanganan add(Penanganan penanganan) {
        return penangananRepository.save(penanganan);
    }

    @Override
    public Penanganan put(Penanganan penanganan, Long id) {
        Penanganan update = penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setNamaPenanganan(penanganan.getNamaPenanganan());
        return penangananRepository.save(update);
    }

    @Override
    public Penanganan get(Long id) {
        return penangananRepository.findById(id).orElseThrow(()-> new NotFoundException("Id not found"));
    }

    @Override
    public List<Penanganan> getAll() {
        return penangananRepository.findAll();
    }

    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            penangananRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
