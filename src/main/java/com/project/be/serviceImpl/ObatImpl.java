package com.project.be.serviceImpl;

import com.project.be.repository.ObatRepository;
import com.project.be.service.ObatService;
import com.project.be.exception.NotFoundException;
import com.project.be.model.Obat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ObatImpl implements ObatService {
    @Autowired
    private ObatRepository obatRepository;

    @Override
    public Obat add(Obat obat) {
        return obatRepository.save(obat);
    }

    @Override
    public Obat put(Obat obat, Long id) {
        Obat update = obatRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setStok(obat.getStok());
        update.setNamaObat(obat.getNamaObat());
        update.setTanggalExpired(obat.getTanggalExpired());
        return obatRepository.save(update);
    }

    @Override
    public Obat get(Long id) {
        return obatRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
    }

    @Override
    public List<Obat> getAll() {
        return obatRepository.findAll();
    }

    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            obatRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
