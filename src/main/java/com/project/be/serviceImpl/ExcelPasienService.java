package com.project.be.serviceImpl;

import com.project.be.model.DataPasien;
import com.project.be.excelModel.ExcelPasien;
import com.project.be.repository.DataPasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelPasienService {
    @Autowired
    private DataPasienRepository dataPasienRepository;

    public ByteArrayInputStream loadSiswa() throws IOException {
        List<DataPasien> pasiens = dataPasienRepository.getAllSiswa();
        ByteArrayInputStream in = ExcelPasien.pasienToExcel(pasiens);
        return in;
    }

    public ByteArrayInputStream loadGuru() throws IOException {
        List<DataPasien> pasiens = dataPasienRepository.getAllGuru();
        ByteArrayInputStream in = ExcelPasien.pasienToExcel(pasiens);
        return in;
    }

    public ByteArrayInputStream loadKaryawan() throws IOException {
        List<DataPasien> pasiens = dataPasienRepository.getAllKaryawan();
        ByteArrayInputStream in = ExcelPasien.pasienToExcel(pasiens);
        return in;
    }

    public void savePasien(MultipartFile file) {
        try {
            List<DataPasien> pasiensList = ExcelPasien.excelToPasien(file.getInputStream());
            dataPasienRepository.saveAll(pasiensList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

}
