package com.project.be.serviceImpl;

import com.project.be.service.UKSService;
import com.project.be.model.Login;
import com.project.be.model.UKSLoginRegister;
import com.project.be.repository.UksRepository;
import com.project.be.exception.InternalErrorException;
import com.project.be.exception.NotFoundException;
import com.project.be.jwt.JwtProvider;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UKSImpl implements UKSService {
    @Autowired
    private UksRepository uksRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return jwtProvider.generateToken(userDetails);
    }

    private String convertToBase64(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String res = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }


    @Override
    public UKSLoginRegister register(UKSLoginRegister uksLoginRegister) {
        if (uksRepository.findByUsername(uksLoginRegister.getUsername()).isPresent())
            throw new InternalErrorException("Username is already");
        String UserPassword = uksLoginRegister.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        uksLoginRegister.setPassword(passwordEncoder.encode(uksLoginRegister.getPassword()));
        return uksRepository.save(uksLoginRegister);
    }

    @Override
    public List<UKSLoginRegister> getAll() {
        return uksRepository.findAll();
    }

    @Override
    public Map<String, Boolean> deleteUser(Long id) {
        try {
            uksRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }

    }

    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getUsername(), login.getPassword());
        UKSLoginRegister uks;
        uks = uksRepository.findByUsername(login.getUsername()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("user", uks);
        return response;

    }

    @Override
    public UKSLoginRegister updateAlamat(Long id, UKSLoginRegister uksLoginRegister) {
        UKSLoginRegister update = uksRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setAlamat(uksLoginRegister.getAlamat());
        return uksRepository.save(update);
    }

    @Override
    public UKSLoginRegister updateTelepon(Long id, UKSLoginRegister uksLoginRegister) {
        UKSLoginRegister update = uksRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setTelepon(uksLoginRegister.getTelepon());
        return uksRepository.save(update);
    }

    @Override
    public UKSLoginRegister updateUsername(Long id, UKSLoginRegister uksLoginRegister) {
        UKSLoginRegister update = uksRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setUsername(uksLoginRegister.getUsername());
        if (uksRepository.findByUsername(uksLoginRegister.getUsername()).isPresent())
            throw new InternalErrorException("Username is already");
        return uksRepository.save(update);
    }

    @Override
    public UKSLoginRegister get(Long id) {
        return uksRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
    }

    @Override
    public UKSLoginRegister updatePass(Long id, UKSLoginRegister uksLoginRegister) {
        UKSLoginRegister update = uksRepository.findById(id).get();
        String UserPassword = uksLoginRegister.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        update.setPassword(passwordEncoder.encode(uksLoginRegister.getPassword()));
        return uksRepository.save(update);
    }

    @Override
    public UKSLoginRegister updatePoto(Long id, UKSLoginRegister uksLoginRegister, MultipartFile multipartFile) {
        UKSLoginRegister update = uksRepository.findById(id).get();
        String url = convertToBase64(multipartFile);
        update.setFoto(url);
        return uksRepository.save(update);
    }

    @Override
    public UKSLoginRegister updateBackground(Long id, UKSLoginRegister uksLoginRegister, MultipartFile multipartFile) {
        UKSLoginRegister update = uksRepository.findById(id).get();
        String url = convertToBase64(multipartFile);
        update.setBackground(url);
        return uksRepository.save(update);
    }
}
