package com.project.be.serviceImpl;

import com.project.be.service.DiagnosaService;
import com.project.be.model.Diagnosa;
import com.project.be.repository.DiagnosaRepository;
import com.project.be.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DiagnosaImpl implements DiagnosaService {
    @Autowired
    private DiagnosaRepository diagnosaRepository;
    @Override
    public Diagnosa add(Diagnosa diagnosa) {
        return diagnosaRepository.save(diagnosa);
    }

    @Override
    public Diagnosa put(Diagnosa diagnosa, Long id) {
        Diagnosa update = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setNamaPenyakit(diagnosa.getNamaPenyakit());
        return diagnosaRepository.save(update);
    }

    @Override
    public Diagnosa get(Long id) {
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
    }

    @Override
    public List<Diagnosa> getAll() {
        return diagnosaRepository.findAll();
    }

    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
