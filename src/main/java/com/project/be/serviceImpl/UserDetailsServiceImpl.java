package com.project.be.serviceImpl;

import com.project.be.model.UKSLoginRegister;
import com.project.be.model.UserPrinciple;
import com.project.be.repository.UksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UksRepository repository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UKSLoginRegister user;
            user = repository.findByUsername(username).get();
        return UserPrinciple.build(user);
    }


}
