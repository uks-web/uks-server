package com.project.be.serviceImpl;

import com.project.be.service.PeriksaService;
import com.project.be.model.Periksa;
import com.project.be.model.PeriksaDTO;
import com.project.be.model.Status;
import com.project.be.model.Tangani;
import com.project.be.repository.DataPasienRepository;
import com.project.be.repository.DiagnosaRepository;
import com.project.be.repository.ObatRepository;
import com.project.be.repository.PenangananRepository;
import com.project.be.repository.PeriksaRepository;
import com.project.be.repository.TindakanRepository;
import com.project.be.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PeriksaServiceImpl implements PeriksaService {
    @Autowired
    PeriksaRepository periksaRepository;

    @Autowired
    DataPasienRepository pasienRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Autowired
    ObatRepository obatRepository;


    @Override
    public Periksa add(PeriksaDTO periksa) {
        Periksa add = new Periksa();
        add.setPasien(pasienRepository.findById(periksa.getPasien()).get());
        add.setKeluhan(periksa.getKeluhan());
        add.setStatusPasien(periksa.getStatusPasien());
        add.setStatus(Status.Belum);
        return periksaRepository.save(add);
    }

    @Override
    public Periksa tangani(Tangani tangani, Long id) {
        Periksa periksa = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException(" Id Not found"));
        periksa.setDiagnosa(diagnosaRepository.findById(tangani.getDiagnosa()).orElseThrow(() -> new NotFoundException("Id not found")));
        periksa.setPenanganan(penangananRepository.findById(tangani.getPenanganan()).orElseThrow(() -> new NotFoundException("Id not found")));
        periksa.setTindakan(tindakanRepository.findById(tangani.getTindakan()).orElseThrow(() -> new NotFoundException("Id not found")));
        periksa.setObat(obatRepository.findById(tangani.getObat()).orElseThrow(() -> new NotFoundException("Id not found")));
        periksa.setStatus(Status.Sudah);
        return periksaRepository.save(periksa);
    }

    @Override
    public Periksa get(Long id) {
        return periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
    }

    @Override
    public List<Periksa> getAll() {
        return  periksaRepository.findAll(Sort.by("tanggal").descending());
    }

    @Override
    public List<Periksa> getStatusPasien(String query) {
        return periksaRepository.findStatusPasien(query);
    }

    @Override
    public List<Periksa> getAllTanggalPeriksa(Date startDate, Date endDate) {
        return periksaRepository.findByTanggalBetween(startDate, endDate);
    }


}
