package com.project.be.serviceImpl;

import com.project.be.excelModel.ExcelPeriksa;
import com.project.be.model.Periksa;
import com.project.be.repository.PeriksaRepository;
import com.project.be.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Service
public class ExcelPeriksaService {
    @Autowired
    PeriksaRepository periksaRepository;

    public ByteArrayInputStream download(Long id) throws IOException {
        Periksa dataPasien = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not found"));
        ByteArrayInputStream in = ExcelPeriksa.periksaToExcel(dataPasien);
        return in;
    }

}
