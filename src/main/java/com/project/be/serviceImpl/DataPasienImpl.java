package com.project.be.serviceImpl;


import com.project.be.model.DataPasien;
import com.project.be.repository.DataPasienRepository;
import com.project.be.service.DataPasienService;
import com.project.be.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataPasienImpl implements DataPasienService {
    @Autowired
    private DataPasienRepository dataPasienRepository;

    @Override
    public DataPasien add(DataPasien dataPasien) {
        return dataPasienRepository.save(dataPasien);
    }

    @Override
    public DataPasien get(Long id) {
        return dataPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
    }

    @Override
    public DataPasien put(DataPasien dataPasien, Long id) {
       DataPasien update = dataPasienRepository.findById(id).orElseThrow(()-> new NotFoundException("Id not found"));
       update.setNama(dataPasien.getNama());
       update.setJabatan(dataPasien.getJabatan());
       update.setAlamat(dataPasien.getAlamat());
       update.setTanggalLahir(dataPasien.getTanggalLahir());
       update.setTempatLahir(dataPasien.getTempatLahir());
       return dataPasienRepository.save(update);
    }

    @Override
    public List<DataPasien> getAllSiswa() {
        return dataPasienRepository.getAllSiswa();
    }

    @Override
    public List<DataPasien> getAllGuru() {
        return dataPasienRepository.getAllGuru();
    }

    @Override
    public List<DataPasien> getAllKaryawan() {
        return dataPasienRepository.getAllKaryawan();
    }

    @Override
    public List<DataPasien> getAllPasien(String query) {
        return dataPasienRepository.findPasien(query);
    }


    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            dataPasienRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }

    }
}
