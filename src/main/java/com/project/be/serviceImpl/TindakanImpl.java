package com.project.be.serviceImpl;

import com.project.be.repository.TindakanRepository;
import com.project.be.service.TindakanService;
import com.project.be.exception.NotFoundException;
import com.project.be.model.Tindakan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TindakanImpl implements TindakanService {
    @Autowired
    private TindakanRepository tindakanRepository;
    @Override
    public Tindakan add(Tindakan tindakan) {
        return tindakanRepository.save(tindakan);
    }

    @Override
    public Tindakan put(Tindakan tindakan, Long id) {
        Tindakan update = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(update);
    }

    @Override
    public Tindakan get(Long id) {
        return tindakanRepository.findById(id).orElseThrow(()-> new NotFoundException("Id not found"));
    }

    @Override
    public List<Tindakan> getAll() {
        return tindakanRepository.findAll();
    }

    @Override
    public Map<String, Boolean> delete(Long id) {
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
