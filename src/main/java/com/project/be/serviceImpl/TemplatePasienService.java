package com.project.be.serviceImpl;

import com.project.be.model.DataPasien;
import com.project.be.excelModel.TemplatePasien;
import com.project.be.repository.DataPasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplatePasienService {
    @Autowired
    DataPasienRepository dataPasienRepository;

    public ByteArrayInputStream templateExcell() {
        List<DataPasien> siswas = dataPasienRepository.getAllSiswa();
        ByteArrayInputStream in = TemplatePasien.templateToExcel(siswas);
        return in;
    }

}
