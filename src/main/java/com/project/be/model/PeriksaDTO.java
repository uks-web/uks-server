package com.project.be.model;

public class PeriksaDTO {
    private Long pasien;

    private String keluhan;

    private String statusPasien;

    public Long getPasien() {
        return pasien;
    }

    public void setPasien(Long pasien) {
        this.pasien = pasien;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public String getStatusPasien() {
        return statusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        this.statusPasien = statusPasien;
    }
}
