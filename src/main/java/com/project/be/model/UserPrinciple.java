package com.project.be.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserPrinciple implements UserDetails {
    private String username;
    private String password;

    private Collection<? extends GrantedAuthority> authority;
    public UserPrinciple(String username, String password) {
        this.username = username;
        this.password = password;
        this.authority = authority;
    }
    public static UserPrinciple build(UKSLoginRegister uksLoginRegister) {
        return new UserPrinciple(
                uksLoginRegister.getUsername(),
                uksLoginRegister.getPassword()
        );
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }
    @Override
    public String getPassword() {
        return password;
    }
    @Override
    public String getUsername() {
        return username;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }



}
