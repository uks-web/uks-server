package com.project.be.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "obat")
public class Obat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_obat")
    private String namaObat;

    @Column(name = "stok")
    private Integer stok;

    @Column(name = "tanggal_expired")
    private Date tanggalExpired;

    public Obat() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public Date getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(Date tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }
}
