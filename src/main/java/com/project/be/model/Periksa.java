package com.project.be.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "periksa_pasien")
public class Periksa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "keluhan")
    private String keluhan;

    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "pasien")
    private DataPasien pasien;

    @ManyToOne
    @JoinColumn(name = "penyakit")
    private Diagnosa diagnosa;

    @ManyToOne
    @JoinColumn(name ="penanganan")
    private Penanganan penanganan;

    @ManyToOne
    @JoinColumn(name = "tindakan")
    private Tindakan tindakan;

    @ManyToOne
    @JoinColumn(name = "obat")
    private Obat obat;


    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column (name = "status_pasien")
    private String statusPasien;

    public Periksa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public DataPasien getPasien() {
        return pasien;
    }

    public void setPasien(DataPasien pasien) {
        this.pasien = pasien;
    }

    public Diagnosa getDiagnosa() {
        return diagnosa;
    }

    public void setDiagnosa(Diagnosa diagnosa) {
        this.diagnosa = diagnosa;
    }

    public Penanganan getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(Penanganan penanganan) {
        this.penanganan = penanganan;
    }

    public Tindakan getTindakan() {
        return tindakan;
    }

    public void setTindakan(Tindakan tindakan) {
        this.tindakan = tindakan;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Obat getObat() {
        return obat;
    }

    public void setObat(Obat obat) {
        this.obat = obat;
    }

    public String getStatusPasien() {
        return statusPasien;
    }

    public void setStatusPasien(String statusPasien) {
        this.statusPasien = statusPasien;
    }
}
