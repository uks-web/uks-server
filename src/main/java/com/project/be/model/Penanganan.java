package com.project.be.model;

import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class Penanganan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_penanganan")
    private String namaPenanganan;

    public Penanganan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenanganan() {
        return namaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        this.namaPenanganan = namaPenanganan;
    }
}
