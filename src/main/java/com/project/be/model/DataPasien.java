package com.project.be.model;

import javax.persistence.*;
import java.util.Date;

//menentukan bahwa kelas adalah entitas dan dipetakan ke tabel database.
@Entity
//digunakan untuk membuat tabel
@Table(name = "data_pasien")
public class DataPasien {
    //    digunakan agar setiap id tidak ada yang sama
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//   digunakan untuk membuat field

    @Column(name = "nama")
    private String nama;

    @Column(name = "jabatan")
    private String jabatan;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    private Date tanggalLahir;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "status")
    private String status;

    public DataPasien() {
    }

    //Getter dan setter digunakan untuk melindungi data Anda, terutama saat membuat kelas. Untuk setiap variabel instan, metode pengambil mengembalikan nilainya sementara metode penyetel menyetel atau memperbarui nilainya. Mengingat hal ini, getter dan setter masing-masing juga dikenal sebagai pengakses dan mutator
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
