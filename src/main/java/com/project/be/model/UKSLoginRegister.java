package com.project.be.model;

import javax.persistence.*;

@Entity
@Table(name = "akun_uks")
public class UKSLoginRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username" )
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "no_telepon")
    private String telepon;

    @Lob
    @Column(name = "alamat")
    private String alamat;
    @Lob
    @Column(name = "foto")
    private String foto;

    @Lob
    @Column(name = "background")
    private String background;
    public UKSLoginRegister() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
