package com.project.be.model;

public class Tangani {
    private Long diagnosa;
    private Long penanganan;
    private Long tindakan;
    private Long obat;

    public Long getDiagnosa() {
        return diagnosa;
    }

    public void setDiagnosa(Long diagnosa) {
        this.diagnosa = diagnosa;
    }

    public Long getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(Long penanganan) {
        this.penanganan = penanganan;
    }

    public Long getTindakan() {
        return tindakan;
    }

    public void setTindakan(Long tindakan) {
        this.tindakan = tindakan;
    }

    public Long getObat() {
        return obat;
    }

    public void setObat(Long obat) {
        this.obat = obat;
    }
}
