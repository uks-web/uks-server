package com.project.be.service;

import com.project.be.model.Login;
import com.project.be.model.UKSLoginRegister;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UKSService {
    UKSLoginRegister register(UKSLoginRegister uksLoginRegister);

    List<UKSLoginRegister> getAll();

    Map<String, Boolean> deleteUser(Long id);

    Map<String, Object> login(Login login);

    UKSLoginRegister updateAlamat(Long id, UKSLoginRegister uksLoginRegister);

    UKSLoginRegister updateTelepon(Long id, UKSLoginRegister uksLoginRegister);

    UKSLoginRegister updateUsername(Long id, UKSLoginRegister uksLoginRegister);


    UKSLoginRegister get(Long id);

    UKSLoginRegister updatePass(Long id, UKSLoginRegister uksLoginRegister);

    UKSLoginRegister updatePoto(Long id,UKSLoginRegister uksLoginRegister , MultipartFile multipartFile);
    UKSLoginRegister updateBackground(Long id,UKSLoginRegister uksLoginRegister , MultipartFile multipartFile);
}
