package com.project.be.service;

import com.project.be.model.Diagnosa;

import java.util.List;
import java.util.Map;

public interface DiagnosaService {
    Diagnosa add(Diagnosa diagnosa);

    Diagnosa put(Diagnosa diagnosa, Long id);

    Diagnosa get(Long id);

    List<Diagnosa> getAll();

    Map<String , Boolean> delete(Long id);
}
