package com.project.be.service;

import com.project.be.model.Periksa;
import com.project.be.model.PeriksaDTO;
import com.project.be.model.Tangani;

import java.util.Date;
import java.util.List;

public interface PeriksaService {
    Periksa add(PeriksaDTO periksa);

    Periksa tangani(Tangani periksa, Long id);

    Periksa get(Long id);

    List<Periksa> getAll();

    List<Periksa> getStatusPasien(String query);

    List<Periksa> getAllTanggalPeriksa(Date startDate, Date endDate);

}
