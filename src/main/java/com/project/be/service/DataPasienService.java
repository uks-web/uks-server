package com.project.be.service;

import com.project.be.model.DataPasien;

import java.util.List;
import java.util.Map;

public interface DataPasienService {
    DataPasien add(DataPasien dataPasien);

    DataPasien get(Long id);

    DataPasien put(DataPasien dataPasien, Long id);

    List<DataPasien> getAllSiswa();
    List<DataPasien> getAllGuru();
    List<DataPasien> getAllKaryawan();

    List<DataPasien> getAllPasien(String query);

    Map<String, Boolean> delete(Long id);
}
