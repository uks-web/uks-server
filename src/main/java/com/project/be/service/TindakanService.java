package com.project.be.service;

import com.project.be.model.Tindakan;

import java.util.List;
import java.util.Map;

public interface TindakanService {
    Tindakan add(Tindakan tindakan);

    Tindakan put(Tindakan tindakan, Long id);

    Tindakan get(Long id);

    List<Tindakan> getAll();

    Map<String, Boolean> delete(Long id);
}
