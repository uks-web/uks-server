package com.project.be.service;

import com.project.be.model.Penanganan;

import java.util.List;
import java.util.Map;

public interface PenangananService {
    Penanganan add(Penanganan penanganan);

    Penanganan put(Penanganan penanganan, Long id);

    Penanganan get(Long id);

    List<Penanganan> getAll();

    Map<String, Boolean> delete(Long id);
}
