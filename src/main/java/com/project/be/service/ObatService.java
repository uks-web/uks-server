package com.project.be.service;

import com.project.be.model.Obat;

import java.util.List;
import java.util.Map;

public interface ObatService {
    Obat add(Obat obat);
    Obat put(Obat obat , Long id);
    Obat get(Long id);
    List<Obat> getAll();
    Map<String, Boolean> delete(Long id);
}
