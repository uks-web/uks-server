package com.project.be.excelModel;

import com.project.be.model.DataPasien;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ExcelPasien {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = {"nama", "jabatan", "tempat lahir", "tanggal lahir", "alamat"};

    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }


    public static ByteArrayInputStream pasienToExcel(List<DataPasien> pasiens) throws IOException {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }


            int rowIdx = 1;
            for (DataPasien pasien : pasiens) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(pasien.getNama());
                row.createCell(1).setCellValue(pasien.getJabatan());
                row.createCell(2).setCellValue(pasien.getTempatLahir());
                row.createCell(3).setCellValue(pasien.getTanggalLahir());
                row.createCell(4).setCellValue(pasien.getAlamat());
            }
                workbook.write(out);
                return new ByteArrayInputStream(out.toByteArray());
            } catch(IOException e){
                throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
            }
        }
        public static List<DataPasien> excelToPasien(InputStream is){

            try {
                Workbook workbook = new XSSFWorkbook(is);

                Sheet sheet = workbook.getSheet(SHEET);
                Iterator<Row> rows = sheet.iterator();

                List<DataPasien> pasienList = new ArrayList<DataPasien>();
                int rowNumber = 0;
                while (rows.hasNext()) {
                    Row currentRow = rows.next();

                    if (rowNumber == 0) {
                        rowNumber++;
                        continue;
                    }

                    Iterator<Cell> cellsInRow = currentRow.iterator();

                    DataPasien pasien = new DataPasien();

                    int cellIdx = 0;
                    while (cellsInRow.hasNext()) {
                        Cell currentCell = cellsInRow.next();

                        switch (cellIdx) {
                            case 0:
                                pasien.setNama(currentCell.getStringCellValue());
                                break;
                            case 1:
                                pasien.setJabatan(currentCell.getStringCellValue());
                                break;
                            case 2:
                                pasien.setTempatLahir(currentCell.getStringCellValue());
                                break;
                            case 3:
                                pasien.setTanggalLahir(currentCell.getDateCellValue());
                                break;
                            case 4:
                                pasien.setAlamat(currentCell.getStringCellValue());
                                break;
                            case 5:
                                pasien.setStatus(currentCell.getStringCellValue());
                                break;

                            default:
                                break;
                        }
                        pasienList.add(pasien);
                        cellIdx++;

                    }
                }
                workbook.close();
                return pasienList;
            } catch (IOException e) {
                throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
            }

        }

}
