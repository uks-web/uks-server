package com.project.be.repository;

import com.project.be.model.Tindakan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanRepository extends JpaRepository<Tindakan, Long> {
}
