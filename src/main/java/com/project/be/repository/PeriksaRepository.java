package com.project.be.repository;


import com.project.be.model.Periksa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PeriksaRepository extends JpaRepository<Periksa , Long> {
    @Query(value = "SELECT * FROM periksa_pasien  WHERE status_pasien = :query", nativeQuery = true)
    List<Periksa> findStatusPasien(String query);

    List<Periksa> findByTanggalBetween(Date startDate, Date endDate);


}
