package com.project.be.repository;

import com.project.be.model.Diagnosa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {
}
