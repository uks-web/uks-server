package com.project.be.repository;

import com.project.be.model.DataPasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataPasienRepository extends JpaRepository<DataPasien , Long> {
//    Untuk membuat query yang nanti akan di gunakan di servie
    @Query(value = "SELECT * FROM data_pasien  WHERE " +
            "status = 'Siswa' ", nativeQuery = true)
    List<DataPasien> getAllSiswa();

    @Query(value = "SELECT * FROM data_pasien  WHERE " +
            "status = 'Guru' ", nativeQuery = true)
    List<DataPasien> getAllGuru();

    @Query(value = "SELECT * FROM data_pasien  WHERE " +
            "status = 'Karyawan' ", nativeQuery = true)
    List<DataPasien> getAllKaryawan();

    @Query(value = "SELECT * FROM data_pasien  WHERE status = :query", nativeQuery = true)
    List<DataPasien> findPasien(String query);


}
