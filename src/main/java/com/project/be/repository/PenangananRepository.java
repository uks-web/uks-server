package com.project.be.repository;

import com.project.be.model.Penanganan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananRepository extends JpaRepository<Penanganan, Long> {
}
