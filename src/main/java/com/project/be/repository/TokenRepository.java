package com.project.be.repository;

import com.project.be.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TemporaryToken ,Long> {
    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findByUksId(long uksId);

}
