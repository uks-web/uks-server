package com.project.be.repository;

import com.project.be.model.UKSLoginRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UksRepository extends JpaRepository<UKSLoginRegister , Long> {
    Optional<UKSLoginRegister> findByUsername(String username);
}
