package com.project.be.exception;

public class EmailCondition extends RuntimeException {
    public EmailCondition(String message) {
        super(message);
    }
}
