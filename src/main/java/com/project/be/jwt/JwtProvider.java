package com.project.be.jwt;

import com.project.be.model.TemporaryToken;
import com.project.be.model.UKSLoginRegister;
import com.project.be.repository.TokenRepository;
import com.project.be.repository.UksRepository;
import com.project.be.exception.InternalErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String secretKey = "Token";
    private static Integer expired = 900000;
    @Autowired
    private TokenRepository temporaryTokenRepository;
    @Autowired
    private UksRepository uksRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        UKSLoginRegister uks = uksRepository.findByUsername(userDetails.getUsername()).get();
        var checkingToken = temporaryTokenRepository.findByUksId(uks.getId());
        if (checkingToken.isPresent()) temporaryTokenRepository.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUksId(uks.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }

}
