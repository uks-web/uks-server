package com.project.be.controller;

import com.project.be.model.DataPasien;
import com.project.be.service.DataPasienService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/data-pasien")
@CrossOrigin(origins = "http://localhost:3000")
public class DataPasienController {
//    Mengunakan autowired agar mudah digunakan tidak perlu memangil satu-satu
    @Autowired
    private DataPasienService dataPasienService;

//   Permintaan post / tambah ke methods
    @PostMapping
    public CommonResponse<DataPasien> add(@RequestBody DataPasien dataPasien) {
        return ResponseHelper.ok(dataPasienService.add(dataPasien));
    }
//Permintaan get by id / mengambil 1 data menggunakan id ke methods
    @GetMapping("/{id}")
    public CommonResponse<DataPasien> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataPasienService.get(id));
    }
//Permintaan put ke methods
    @PutMapping("/{id}")
    public CommonResponse<DataPasien> put(@RequestBody DataPasien dataPasien, @PathVariable("id") Long id) {
        return ResponseHelper.ok(dataPasienService.put(dataPasien, id));
    }
//Permintaan get all siswa ke methods
    @GetMapping("/siswa")
    public CommonResponse<List<DataPasien>> getAllSiswa() {
        return ResponseHelper.ok(dataPasienService.getAllSiswa());
    }
    //Permintaan get all guru ke methods
    @GetMapping("/guru")
    public CommonResponse<List<DataPasien>> getAllGuru() {
        return ResponseHelper.ok(dataPasienService.getAllGuru());
    }
    //Permintaan get all karyawan ke methods
    @GetMapping("/karyawan")
    public CommonResponse<List<DataPasien>> getAllKaryawan() {
        return ResponseHelper.ok(dataPasienService.getAllKaryawan());
    }
    //Permintaan get all  ke methods
    @GetMapping("/all")
    public CommonResponse<List<DataPasien>> getAllPasien(@RequestParam(name = "status") String query) {
        return ResponseHelper.ok(dataPasienService.getAllPasien(query));
    }
//Permintaan delete by id ke methods
    @DeleteMapping("/{id}")
    public CommonResponse<?> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(dataPasienService.delete(id));
    }
}
