package com.project.be.controller;

import com.project.be.model.Login;
import com.project.be.model.UKSLoginRegister;
import com.project.be.service.UKSService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/uks")
@CrossOrigin(origins = "http://localhost:3000")
public class UKSLoginRegisterController {
    @Autowired
    private UKSService uksService;

    @PostMapping("/register")
    public CommonResponse<UKSLoginRegister> registrasi(@RequestBody UKSLoginRegister uksLoginRegister) {
        return ResponseHelper.ok(uksService.register(uksLoginRegister));
    }

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.ok(uksService.login(login));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteUser(@PathVariable("id") Long id) {
        return ResponseHelper.ok(uksService.deleteUser(id));
    }
    @PutMapping("/update-alamat/{id}")
    public CommonResponse <UKSLoginRegister> UpdateAlamat(@PathVariable("id") Long id, @RequestBody UKSLoginRegister uksLoginRegister) {
        return ResponseHelper.ok(uksService.updateAlamat(id,uksLoginRegister));
    }
    @PutMapping("/update-telepon/{id}")
    public CommonResponse <UKSLoginRegister> UpdateTelepon(@PathVariable("id") Long id, @RequestBody UKSLoginRegister uksLoginRegister) {
        return ResponseHelper.ok(uksService.updateTelepon(id,uksLoginRegister));
    }
    @PutMapping("/update-username/{id}")
    public CommonResponse <UKSLoginRegister> UpdateUsername(@PathVariable("id") Long id, @RequestBody UKSLoginRegister uksLoginRegister) {
        return ResponseHelper.ok(uksService.updateUsername(id,uksLoginRegister));
    }
    @PutMapping(path = "/update-foto/{id}", consumes = "multipart/form-data")
    public CommonResponse <UKSLoginRegister> UpdateFoto(@PathVariable("id") Long id,UKSLoginRegister uksLoginRegister,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(uksService.updatePoto(id,uksLoginRegister,multipartFile));
    }
    @PutMapping(path = "/update-background/{id}", consumes = "multipart/form-data")
    public CommonResponse <UKSLoginRegister> UpdateBackground(@PathVariable("id") Long id,UKSLoginRegister uksLoginRegister,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(uksService.updateBackground(id,uksLoginRegister,multipartFile));
    }
    @PutMapping("/update-password/{id}")
    public CommonResponse <UKSLoginRegister> UpdatePassword(@PathVariable("id") Long id, @RequestBody UKSLoginRegister uksLoginRegister) {
        return ResponseHelper.ok(uksService.updatePass(id,uksLoginRegister));
    }

    @GetMapping
    public CommonResponse<List<UKSLoginRegister>> getAll() {
        return ResponseHelper.ok(uksService.getAll());
    }
    @GetMapping("/{id}")
    public CommonResponse<UKSLoginRegister> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(uksService.get(id));
    }




}
