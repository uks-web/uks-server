package com.project.be.controller;

import com.project.be.model.Periksa;
import com.project.be.model.PeriksaDTO;
import com.project.be.model.Tangani;
import com.project.be.service.PeriksaService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/periksa")
@CrossOrigin(origins = "http://localhost:3000")
public class PeriksaController {
    @Autowired
    PeriksaService periksaService;

    @PostMapping
    public CommonResponse<Periksa> addPasien(@RequestBody PeriksaDTO periksa) {
        return ResponseHelper.ok(periksaService.add(periksa));
    }

    @PutMapping("/tangani/{id}")
    public CommonResponse<Periksa> tangani(@RequestBody Tangani tangani, @PathVariable("id") Long id) {
        return ResponseHelper.ok(periksaService.tangani(tangani, id));
    }

    @GetMapping("/{id}")
    public CommonResponse<Periksa> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(periksaService.get(id));
    }

    @GetMapping
    public CommonResponse<List<Periksa>> getAll() {
        return ResponseHelper.ok(periksaService.getAll());
    }

    @GetMapping("/all")
    public CommonResponse<List<Periksa>> getStatusPasien(@RequestParam(name = "status_pasien") String query) {
        return ResponseHelper.ok(periksaService.getStatusPasien(query));
    }

    @GetMapping("/tanggal")
    public CommonResponse<List<Periksa>> getAllTanggalPeriksa(@RequestParam(name = "start_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate, @RequestParam(name = "end_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {

        return ResponseHelper.ok(periksaService.getAllTanggalPeriksa(startDate, endDate));
    }


}
