package com.project.be.controller;

import com.project.be.model.Obat;
import com.project.be.service.ObatService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/obat")
@CrossOrigin(origins = "http://localhost:3000")
public class ObatController {
    @Autowired
    private ObatService obatService;

    @PostMapping
    public CommonResponse<Obat> add(@RequestBody Obat obat) {
        return ResponseHelper.ok(obatService.add(obat));
    }

    @PutMapping("/{id}")
    public CommonResponse<Obat> put(@RequestBody Obat obat, @PathVariable("id") Long id) {
        return ResponseHelper.ok(obatService.put(obat, id));
    }

    @GetMapping("/{id}")
    public CommonResponse<Obat> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(obatService.get(id));
    }

    @GetMapping
    public CommonResponse<List<Obat>> getAll() {
        return ResponseHelper.ok(obatService.getAll());
    }

    @DeleteMapping("/{id}")
    public CommonResponse<?> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(obatService.delete(id));
    }
}
