package com.project.be.controller;

import com.project.be.model.Penanganan;
import com.project.be.service.PenangananService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/penanganan")
@CrossOrigin(origins = "http://localhost:3000")
public class PenangananController {
    @Autowired
    private PenangananService penangananService;

    @PostMapping
    public CommonResponse<Penanganan> add(@RequestBody Penanganan penanganan) {
        return ResponseHelper.ok(penangananService.add(penanganan));
    }
    @PutMapping("/{id}")
    public CommonResponse<Penanganan> put(@RequestBody Penanganan penanganan , @PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.put(penanganan, id));
    }
    @GetMapping("/{id}")
    public CommonResponse<Penanganan> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.get(id));
    }
    @GetMapping
    public CommonResponse<List<Penanganan>> getAll() {
        return ResponseHelper.ok(penangananService.getAll());
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.delete(id));
    }
}
