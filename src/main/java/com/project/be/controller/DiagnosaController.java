package com.project.be.controller;

import com.project.be.model.Diagnosa;
import com.project.be.service.DiagnosaService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/diagnosa")
@CrossOrigin(origins = "http://localhost:3000")
public class DiagnosaController {
    @Autowired
    private DiagnosaService diagnosaService;

    @PostMapping
    public CommonResponse<Diagnosa> add(@RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.add(diagnosa));
    }
    @PutMapping("/{id}")
    public CommonResponse<Diagnosa> put(@RequestBody Diagnosa diagnosa , @PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.put(diagnosa, id));
    }
    @GetMapping("/{id}")
    public CommonResponse<Diagnosa> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.get(id));
    }
    @GetMapping
    public CommonResponse<List<Diagnosa>> getAll() {
        return ResponseHelper.ok(diagnosaService.getAll());
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.delete(id));
    }
}
