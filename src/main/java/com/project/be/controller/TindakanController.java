package com.project.be.controller;

import com.project.be.model.Tindakan;
import com.project.be.service.TindakanService;
import com.project.be.response.CommonResponse;
import com.project.be.response.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/tindakan")
@CrossOrigin(origins = "http://localhost:3000")
public class TindakanController {
    @Autowired
    private TindakanService tindakanService;

    @PostMapping
    public CommonResponse<Tindakan> add(@RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.add(tindakan));
    }

    @PutMapping("/{id}")
    public CommonResponse<Tindakan> put(@RequestBody Tindakan tindakan, @PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.put(tindakan, id));
    }

    @GetMapping("/{id}")
    public CommonResponse<Tindakan> get(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.get(id));
    }

    @GetMapping
    public CommonResponse<List<Tindakan>> getAll() {
        return ResponseHelper.ok(tindakanService.getAll());
    }

    @DeleteMapping("/{id}")
    public CommonResponse<?> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.delete(id));
    }
}
