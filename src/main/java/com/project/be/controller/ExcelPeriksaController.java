package com.project.be.controller;

import com.project.be.serviceImpl.ExcelPeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/periksa-excel")
@CrossOrigin(origins = "http://localhost:3000")
public class ExcelPeriksaController {
    @Autowired
    ExcelPeriksaService excelPeriksaService;

    @GetMapping("/download")
    public ResponseEntity<Resource> getFile(Long id) throws IOException {
        String filename = "data-periksa.xlsx";
        InputStreamResource file = new InputStreamResource(excelPeriksaService.download(id));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
}
