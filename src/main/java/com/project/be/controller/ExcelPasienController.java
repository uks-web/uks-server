package com.project.be.controller;

import com.project.be.excelModel.ExcelPasien;
import com.project.be.model.ResponseMessage;
import com.project.be.serviceImpl.ExcelPasienService;

import com.project.be.serviceImpl.TemplatePasienService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

//memproses permintaan REST API yang masuk, menyiapkan model, dan mengembalikan tampilan untuk dirender sebagai respons
@RestController
@RequestMapping("/pasien-excel")
@CrossOrigin(origins = "http://localhost:3000")
public class ExcelPasienController {
    @Autowired
    ExcelPasienService excelPasienService;

    @Autowired
    TemplatePasienService templateExcell;

    @GetMapping("/download/siswa")
    public ResponseEntity<Resource> getFileSiswa() throws IOException {
        String filename = "data-siswa.xlsx";
        InputStreamResource file = new InputStreamResource(excelPasienService.loadSiswa());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
    @GetMapping("/download/guru")
    public ResponseEntity<Resource> getFileGuru() throws IOException {
        String filename = "data-guru.xlsx";
        InputStreamResource file = new InputStreamResource(excelPasienService.loadGuru());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }
    @GetMapping("/download/karyawan")
    public ResponseEntity<Resource> getFileKaryawan() throws IOException {
        String filename = "data-karyawan.xlsx";
        InputStreamResource file = new InputStreamResource(excelPasienService.loadKaryawan());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download/template")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "template.xlsx";
        InputStreamResource file = new InputStreamResource(templateExcell.templateExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }


    @PostMapping(path = "/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestPart("file") MultipartFile file) {
        String message = "";
        if (ExcelPasien.hasExcelFormat(file)) {
            try {
                excelPasienService.savePasien(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
